//
//サンプルコードをガバガバに合成して出来上がったもの
//wificonfig + Webserver　実装（してるはず）
//arduinoOTA実装

#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino

//needed for library
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>         //https://github.com/tzapu/WiFiManager
#include <ESP8266mDNS.h>
//OTA
#include <WiFiUdp.h>
#include <ArduinoOTA.h>


//Webサーバーの生成。
ESP8266WebServer server(80);

//アクセスLED定義 ※arduino互換ボードのLEDは2番
const int led = 2;

//ブレッドボードのLED
const int LEDoutput = 5;

//サーバリクエスト受信時の正常処理 ルート
void handleRoot() {
  digitalWrite ( led, 1 );

//HTML
  String msg = 
"<html>"
"<head>"
"<title>ESP8266WebSerber</title>"
"</head>"
"<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>"
"<body bgcolor=#cccccc>"
"<div align='center'>"
"<h1>ESP8266Webガバ鯖</h1>"
"ガバガバ遠隔LEDチカチカプログラム<br />"
"<form action=ON><input type= submit  value=ON  target=tif></form>"
"<form action=gaba><input type= submit  value=gaba  target=tif></form>"
"</div>"
"</body>"
"</html>";
  
  server.send ( 200, "text/html", msg );
  digitalWrite ( led, 0 );
}

//サーバリクエスト受信時の異常処理
//定義されたURI以外にアクセスされた場合の処理を書く
void handleNotFound(){
  
  digitalWrite(led, 1);
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i=0; i<server.args(); i++){
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
  digitalWrite(led, 0);
}



void setup() {
    pinMode(LEDoutput,OUTPUT);
    pinMode(led, OUTPUT);
    digitalWrite(led, 0);
    Serial.begin(115200);
    Serial.println("");

//ここはオートコンフィグ用の記述
  
    // put your setup code here, to run once:
    Serial.begin(115200);

    //WiFiManager
    //Local intialization. Once its business is done, there is no need to keep it around
    WiFiManager wifiManager;
    //reset saved settings
    //wifiManager.resetSettings();
    
    //set custom ip for portal 　たぶんIP固定してくれる ... ハズなんだけどなんかうまくいかね
    //Sets a custom ip / gateway / subnet configuration
    //↑こうらしい
    //wifiManager.setAPConfig(IPAddress(192,168,1,51), IPAddress(192,168,1,1), IPAddress(255,255,255,0));

    //IPAddress ip(192,168,1,5); 
    //IPAddress gw(192,168,1,1); 
    //IPAddress sn(255,255,255,0); 
    //WiFiManager.setAPConfig(ip, gw, sn);


    //fetches ssid and pass from eeprom and tries to connect
    //if it does not connect it starts an access point with the specified name
    //here  "AutoConnectAP"
    //and goes into a blocking loop awaiting configuration
    wifiManager.autoConnect("AutoConnectAP");
    //or use this for auto generated name ESP + ChipID
    //wifiManager.autoConnect();

    
    //if you get here you have connected to the WiFi
    Serial.println("Yattane!connected...yeey :)");

//オートコンフィグ用の記述ここまで


//arduinoOTA
   Serial.println("ArduinoOTABooting");
   //アクセスポイント機能を切るらしい
   WiFi.mode(WIFI_STA);
  //コネクション張れてると思うけど、一応
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
      delay(5000);
      ESP.restart();
  }

  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
  // ArduinoOTA.setHostname("myesp8266");

  // No authentication by default
  // ArduinoOTA.setPassword((const char *)"123");

  ArduinoOTA.onStart([]() {
    Serial.println("Start");
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
//OTAここまで      
    
//webサーバ
    //一応デバックのため表示
    Serial.println(WiFi.localIP());

    //もしかしたらいらないかも
    if (MDNS.begin("esp8266")) {
    Serial.println("MDNS responder started");
    }
    
    //Webサーバの設定を行い、サーバを起動する
    server.on("/", handleRoot);//ルートに接続要求があった時の処理を指定
    server.on("/ON", ON_command);
    server.on("/gaba", gaba_command);
    
    server.on("/inline", [](){
      server.send(200, "text/plain", "this works as well");//Webサーバの接続要求待ち
    });

    server.onNotFound(handleNotFound);

    //サーバーを起動する.
    server.begin();
    Serial.println("HTTP server started");
//webサーバここまで    
}

void loop() {
    // put your main code here, to run repeatedly:

    //ArduinoOTA
    ArduinoOTA.handle();

    //webサーバ
    server.handleClient();
      
}

//フォームボタン処理
void ON_command() {
  Serial.println("ON_button has been pressed!");
  //server.send(200, "text/html","LED ON!!!!");
  delay(1000);
  digitalWrite ( led, 0 );
  delay(1000);
  digitalWrite ( led, 1 );
  delay(1000);
  digitalWrite ( led, 0 );
  delay(1000);
  handleRoot();
}

void gaba_command() {
  Serial.println("gaba_button has been pressed!");
  //server.send(200, "text/html",msg);
  delay(100);
  digitalWrite(5, HIGH);
  delay(100);         
  digitalWrite(5, LOW);
  delay(100); 
  handleRoot();
}
